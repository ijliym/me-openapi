/*
 * Copyright 2011-2018 ijym-lee
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * OAuth 2.0 配置
 *
 * <p>https://blog.csdn.net/lixiang987654321/article/details/83381494</p>
 * <p>https://www.cnblogs.com/xingxueliao/p/5911292.html</p>
 *
 * @author liym
 * @see org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationProcessingFilter#doFilter
 * @see org.springframework.security.web.authentication.www.BasicAuthenticationFilter#doFilterInternal
 * @see org.springframework.security.authentication.ProviderManager
 * @see org.springframework.security.oauth2.provider.endpoint.FrameworkEndpointHandlerMapping
 * @since 2018-11-30 15:18 新建
 */
package me.ijleex.openapi.oauth.config;
