/*
 * Copyright 2011-2018 ijym-lee
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.ijleex.openapi.oauth.entity;

import java.util.Collections;
import java.util.List;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

/**
 * 用户详情
 *
 * @author liym
 * @since 2018-12-25 00:06 新建
 */
public class UserDetails extends User {

    private static final long serialVersionUID = -5501855210686683333L;
    private AccessUser user;

    public UserDetails(AccessUser user) {
        super(user.getUserNo(), user.getPassword(), true, true, true, true, Collections.emptySet());
        this.user = user;
    }

    public UserDetails(AccessUser user, List<SimpleGrantedAuthority> authorities) {
        super(user.getUserNo(), user.getPassword(), true, true, true, true, authorities);
        this.user = user;
    }

    public AccessUser getUser() {
        return user;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

}
